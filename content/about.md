+++
menu = "main"
title = "About"
type = "about"
weight = 10
+++

A number of years back, I began a career in college teaching after entering candidacy for a Ph.D, in Sociology. A number of events "conspired" to push and pull me from that career into sales--eventually life, and health, and retirement planning.

I still consider myself a teacher more than a salesman. However like many of my clients, I discovered after the death of my wife and the loss of that income that I had not invested sufficiently in my retirement years. As I work to remedy that situation, I think I am uniquely qualified to talk with other seniors about meeting senior issues in their senior years.

I can say with certainty that it is never too soon and never too late to start planning.

![Creative Insurance Solutions](/images/creative_insurance_solutions.png)
